from datetime import datetime
import pygame
import random
import json
from enum import Enum
from collections import namedtuple


pygame.init()

BLOCK_SIZE = 20
SPEED = 10
# rgb colors
WHITE = (255, 255, 255)
RED = (200, 0, 0)
GREEN1 = (0, 128, 0)
GREEN2 = (124, 252, 0)
BLACK = (0, 0, 0)

font = pygame.font.SysFont('arial', 25)
class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


Point = namedtuple('Point', 'x, y')

with open("score.json", "r") as r_file:
      list_score = json.load(r_file)
      highest_score = max([max(dict.values()) for dict in list_score])
class SnakeGame:

    def __init__(self, width=800, height=600):
        self.width = width
        self.height = height
        # init display
        self.display = pygame.display.set_mode((self.width, self.height))
        # set time speed
        self.clock = pygame.time.Clock()
        # init game state
        self.direction = Direction.RIGHT
        self.head = Point(self.width/2, self.height/2)
        self.snake = [self.head,
                      Point(self.head.x-BLOCK_SIZE, self.head.y),
                      Point(self.head.x-(2*BLOCK_SIZE), self.head.y)]
        self.score = 0
        self.food = None
        self._generate_food()

    def _generate_food(self):
        x = random.randint(0, (self.width-BLOCK_SIZE)//BLOCK_SIZE)*BLOCK_SIZE
        y = random.randint(0, (self.height-BLOCK_SIZE)//BLOCK_SIZE)*BLOCK_SIZE
        self.food = Point(x, y)
        if self.food in self.snake:
            self._generate_food()

    def play_game(self):
        # 1. collect user input
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.direction = Direction.LEFT
                elif event.key == pygame.K_RIGHT:
                    self.direction = Direction.RIGHT
                elif event.key == pygame.K_UP:
                    self.direction = Direction.UP
                elif event.key == pygame.K_DOWN:
                    self.direction = Direction.DOWN

        # 2. move
        self._move(self.direction)
        self.snake.insert(0, self.head)

        # 3. check if game over, and saves score if true
        game_over = False
        if self._is_crashed():
            game_over = True
            self._save_score(score)
            # self.score_screen(score)
            return game_over, self.score

        # 4. place new food or just move
        if self.head == self.food:
            self.score += 1
            self._generate_food()
        else:
            self.snake.pop()

        # 5. update ui and clock
        self._update_frame()
        self.clock.tick(SPEED)
        # 6. return game over and score
        return game_over, self.score

    def _is_crashed(self):
        # hits boundary
        if self.head.x > self.width - BLOCK_SIZE \
        or self.head.x < 0 \
        or self.head.y > self.height - BLOCK_SIZE \
        or self.head.y < 0:
            return True
        # hits itself
        if self.head in self.snake[1:]:
            return True
        return False

    def _update_frame(self):
        self.display.fill(BLACK)
        for pt in self.snake:
            pygame.draw.rect(self.display, GREEN1, pygame.Rect(
                pt.x, pt.y, BLOCK_SIZE, BLOCK_SIZE))
            pygame.draw.rect(self.display, GREEN2,
                            pygame.Rect(pt.x+4, pt.y+4, 12, 12))

        pygame.draw.rect(self.display, RED, pygame.Rect(
            self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))

        highest_score_text = font.render("Highest score: " + str(highest_score), True, WHITE)
        self.display.blit(highest_score_text, [0, 0])
        current_score_text = font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(current_score_text, [0, 30])
        
        pygame.display.flip()

    def _move(self, direction):
        x = self.head.x
        y = self.head.y
        if direction == Direction.RIGHT:
            x += BLOCK_SIZE
        elif direction == Direction.LEFT:
            x -= BLOCK_SIZE
        elif direction == Direction.DOWN:
            y += BLOCK_SIZE
        elif direction == Direction.UP:
            y -= BLOCK_SIZE
        self.head = Point(x, y)

    def _save_score(self, score):
        now = datetime.now().strftime("%d-%m-%Y-%H:%M:%S")
        with open("score.json", "r") as r_file:
            list_score = json.load(r_file)
        with open("score.json", 'w') as w_file:
            list_score.append({ now : score})
            json.dump(list_score,w_file)

    def score_screen(self, score):
        self.display.fill(BLACK)





if __name__ == '__main__':
    game = SnakeGame()
    # game loop
    while True:
        game_over, score = game.play_game()

        if game_over == True:
            break

    print('Final Score', score)

    pygame.quit()
